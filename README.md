# Getting Started

## Installation Instructions

```
$ npm install
```

## Usage

```
$ grunt serve
```

## Config

This package allows you to choose a basic PHP or CodeIgniter file structure dependeing on what you prefer. You can modify your preferences in the `Gruntfile.js`

```
appBase: config.basic,
```

# Resources

During this hackathon, you may need access to some resources to help build out your application. Here are a couple to get you started

## APIs

### Travel Pass Data:

```
http://www.telus.com/services/v2/addons/travel/pass?language=en&type=all
```
Params:

 + language (en | fr)
 + type (all | consumer | business)

### Device Catalog
```
http://www.telus.com/en/on/mobility/catalog/?ajax
```

Params:
  
  + None

### Accessories Catalog

#### Category Information
```
http://www.telus.com/services/accessories/catalog/getCategories?language=en
```

#### Accessory Information
```
http://www.telus.com/services/accessories/catalog/getAll?language=en
```

Params:

 + language (en | fr)

 
## Data Files

### Store Locator Data

Inside the resources folder you'll find a `stores.json` file which will include all the data 


# Help

## What's included

+ Automatic PHP Server (from $PATH)
+ BrowserSync
+ Libsass
+ Basic PHP or CodeIgniter Framework

## FAQ

### HELP! I'm running windows

If you're running windows, you'll need to install PHP and load it into your PATH variable. Grab Wasim for some help

