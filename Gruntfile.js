'use strict';
module.exports = function (grunt) {

  var config = {
    basic: 'app/basic', // Basic PHP app
    ci: 'app/ci' // CI app
  };

  grunt.initConfig({
    
    appBase: config.basic,

    watch: {
      sass: {
        files: '<%= appBase %>/src/scss/**/*.scss',
        tasks: ['sass:dist']
      },
      js: {
        files: '<%= appBase %>/src/js/**/*.js',
        tasks: ['clean:js','copy:js']
      }      
    },
    php: {
      serve: {
        options: {
          hostname: '127.0.0.1',
          port: 9000,
          base: '<%= appBase %>',
          keepalive: false,
          open: false
        }
      }
    },

    copy: {
      js: {
        files: [
          {
            expand: true, 
            cwd: '<%= appBase %>/src/js/', 
            src: ['**'], 
            dest: '<%= appBase %>/dist/js/'
          },
        ],
      },
    },  

    clean: {
      js: ["<%= appBase %>/dist/js/**/*.js"]
    },

    // JAVASCRIPT

    jscs: {
        src: "<%= appBase %>/src/js/**/*.js",
        options: {
            config: ".jscsrc",
            esnext: false, // If you use ES6 http://jscs.info/overview.html#esnext
            verbose: true, // If you need output with rule names http://jscs.info/overview.html#verbose
            requireCurlyBraces: [ "if" ]
        }
    },    


    // SASS
    sass: {
        options: {
            sourceMap: true
        },
        dist: {
          options: {
            sourceMap: false,
            outputStyle: 'compressed'
          },
          files: [{
            expand: true,
            cwd: '<%= appBase %>/src/scss/',
            src: ['**/*.scss'],
            dest: '<%= appBase %>/dist/css/',
            ext: '.css'
          }]       
        }        
    },

    browserSync: {
      serve: {
        bsFiles: {
          src: ['<%= appBase %>/**/*']
        },
        options: {
          proxy: '<%= php.serve.options.hostname %>:<%= php.serve.options.port %>',
          watchTask: true,
          notify: true,
          open: true,
          logLevel: 'silent',
          ghostMode: {
            clicks: true,
            scroll: true,
            links: true,
            forms: true
          }
        }
      }
    }
  });

  grunt.loadTasks('tasks');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks("grunt-jscs");
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browser-sync');

  grunt.registerTask('serve', [
    'php:serve',
    'browserSync:serve',
    'watch'
  ]);


};
