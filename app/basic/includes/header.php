<header class="site-header">
  <div class="grid">

    <div class="telus-logo" title="Return to the TELUS Homepage">
      <a href="http://www.telus.com" title="Back to Homepage">
        <img src="//static.telus.com/common/images/header/TELUS-logo.svg" alt="TELUS Logo">
      </a>
    </div>

    <nav>

      <!-- HEADER GOES HERE -->

      <!-- Channel select -->
      <ul class="hacker-channels">
        <li class="hacker-channels__item"><a href="#">Personal</a></li>
         | 
        <li class="hacker-channels__item"><a href="#">Business</a></li>
      </ul>

      <!-- Buttons -->
      <ul class="hacker-nav tg-row">
        <li class="hacker-nav__item tg-col-sm-1"><a href="#">Products</a></li>
        <li class="hacker-nav__item tg-col-sm-1"><a href="#">Login</a></li>
        <li class="hacker-nav__item tg-col-sm-1"><a href="#">Support</a></li>
      </ul>
      










    </nav>


<!-- <nav role="navigation" class="meganav meganav-east ">

  <a href="#meganav-l1" class="meganav-toggle meganav-hamburger show-for-mobile" title="Show menu">
    <span class="visually-hidden">Show menu</span>
    <span class="frg-icon icon-hamburger"></span>
  </a>

    <ul id="meganav-l1" class="meganav-l1">

    <li class="meganav-homelink hide-for-tablet-up">
      <a href="/" class="meganav-l1-link" title="Home">
        Home
      </a>
    </li>

    <li class="meganav-shoplink">
      <a href="#meganav-l2-shop" class="meganav-toggle meganav-l1-link" title="Shop">
        Shop
        <span class="frg-icon icon-plus hide-for-tablet-up toggle-icon"></span>
      </a>

            <div id="meganav-l2-shop" class="meganav-l2 meganav-shop">
        <ul class="meganav-l2-inner">

          <li class="meganav-shop-wireless meganav-l2-section">

                        <a href="#meganav-l3-wireless" class="meganav-toggle hide-for-tablet-up">
              <span class="frg-icon icon-smartphone-inverted"></span>
              Mobility
              <span class="frg-icon icon-plus hide-for-tablet-up toggle-icon"></span>
            </a>

                        <a href="http://www.telus.com/mobility?INTCMP=LNK_frmCTA_TopNavigationAll_toMobility_BetaSS" class="hide-for-mobile">
              <span class="frg-icon icon-smartphone-inverted"></span>
              Mobility
              <span class="frg-icon icon-plus hide-for-tablet-up toggle-icon"></span>
            </a>

                        <div id="meganav-l3-wireless" class="meganav-l3">
              <ul class="meganav-l3-sub">
                <li class="hide-for-tablet-up"><a href="http://www.telus.com/mobility">Mobility Overview</a></li>
                <li><a href="http://www.telus.com/mobility/catalog?INTCMP=LNK_frmCTA_TopNavigationAll_toPhone_BetaSS">Phones</a></li>
                <li><a href="http://www.telus.com/mobility/accessories?INTCMP=LNK_frmCTA_TopNavigationAll_toAccessories_BetaSS">Accessories</a></li>
                <li><a href="http://www.telus.com/mobility/sim-cards?INTCMP=LNK_frmCTA_TopNavigationAll_toSIMCard_BetaSS">SIM Cards</a></li>
                <li><a href="http://www.telus.com/mobility/tablets?INTCMP=LNK_frmCTA_TopNavigationAll_toTablets_BetaSS">Tablets</a></li>
              </ul>
              <ul class="meganav-l3-sub">
                <li><a href="http://www.telus.com/mobility/plans?INTCMP=LNK_frmCTA_TopNavigationAll_toPlans_BetaSS">Plans</a></li>
                <li><a href="http://www.telus.com/mobility/prepaid?INTCMP=LNK_frmCTA_TopNavigationAll_toPrepaid_BetaSS">Prepaid</a></li>
                <li><a href="http://www.telus.com/mobility/add-ons/index.jsp?INTCMP=LNK_frmCTA_TopNavigationAll_toAddonsAndApps_BetaSS">Add-ons &amp; Apps</a></li>
                <li><a href="http://www.telus.com/mobility/network?INTCMP=LNK_frmCTA_TopNavigationAll_toNetwork_BetaSS">Network &amp; coverage</a></li>
                <li><a href="http://www.telus.com/mobility/travel?zone=us&amp;country=USA&amp;INTCMP=LNK_frmCTA_TopNavigationAll_toTravel_BetaSS">Travel</a></li>
                <li><a href="http://www.telus.com/mobility/services?INTCMP=LNK_frmCTA_TopNavigationAll_toServices_BetaSS">Exclusive services</a></li>
              </ul>
            </div>
          </li>


            
            <li class="meganav-shop-mobileinternet meganav-l2-section">
              <a href="http://www.telus.com/mobility/mobile-internet/">
                <span class="frg-icon icon-usb-stick-inverted"></span>
                Mobile Internet
              </a>
              <div class="meganav-copy hide-for-mobile meganav-copy-mobileinternet">
                <p>See our most popular mobile Internet keys, Hotspots, and Smart hubs.</p>
                <a href="http://www.telus.com/mobility/mobile-internet/">Learn more <span class="frg-icon icon-arrow-right-circled" aria-hidden="true"></span></a>
              </div>


            </li>


          <li class="meganav-shop-deals meganav-l2-section">
            <a href="//www.telus.com/deals/?INTCMP=LNK_frmCTA_TopNavigationAll_toDeals_BetaSS">
              <span class="frg-icon icon-pricetag-inverted"></span>
              Deals
            </a>
            <div class="meganav-copy hide-for-mobile">
                  <p>Now's the time to get the hottest smartphones on a 2 year term.</p>
                  <img src="//static.telus.com/common/images/nav/deals-smartphones.jpg" alt="Smartphones" class="featured-deal-img">
                  <a href="http://www.telus.com/deals/?INTCMP=LNK_frmCTA_TopNavigationAll_toDeals_BetaSS">See all deals <span class="frg-icon icon-arrow-right-circled" aria-hidden="true"></span></a>
            </div>
          </li>

                    <li class="hide-for-tablet-up">
              <a href="/my-account/?INTCMP=MegaNavsslogin">
                <span class="frg-icon icon-user-profile-inverted"></span>
                My Account
              </a>
          </li>

        </ul>

                  <div class="meganav-footer">
    <ul class="meganav-footer-top hide-for-tablet-up">
      <li><a href="/get-help">Get Help</a></li>
      <li><a href="http://about.telus.com/community/english">About us</a></li>
      <li><a href="http://business.telus.com/">Business</a></li>
    </ul>
    <ul class="meganav-footer-bottom">
      <li>
        <a href="http://givewherewelive.ca/">We give where we live™</a>
        <img alt="" src="//static.telus.com/common/images/nature/gazania-yellow.png">
      </li>
      <li class="meganav-desktop-about"><a href="http://about.telus.com/community/english">About us</a></li>
    </ul>
  </div>


      </div>
    </li>

    <li class="meganav-accountlink hide-for-mobile">
        <a href="/my-account/?INTCMP=MegaNavsslogin" class="meganav-l1-link hide-for-mobile" title="My Account">
          My Account
        </a>
    </li>

    <li class="meganav-helplink hide-for-mobile">
      <a href="http://www.telus.com/get-help?INTCMP=LNK_frmCTA_TopNavigationAll_toGetHelp_BetaSS" class="meganav-l1-link" title="Get Help">
        Get Help
      </a>
    </li>

    <li class="meganav-businesslink hide-for-mobile">
      <a href="http://business.telus.com/?INTCMP=LNK_frmCTA_TopNavigationAll_business" class="meganav-l1-link" title="Business">
        Business
      </a>
    </li>

  </ul>

</nav> -->


<!-- <div class="header-icons ">


  <a title="Search" href="#" class="icon-holder search-toggle" id="header-search">
    <span aria-hidden="true" role="presentation" class="frg-icon icon-magnify-glass color-purple"></span>
    <span class="visually-hidden">Search</span>
  </a>

  <span class="site-settings-toggle icon-holder">
    <a href="#" title="">
      <span class="frg-icon icon-map-marker color-purple"></span>
      <span class="region-language-text">
        <abbr title="en/ON" lang="en">en/<span class="region">ON</span></abbr>
      </span>
    </a>
  </span>


</div>
-->
</div>
</header>