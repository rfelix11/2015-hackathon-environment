require(['sandboxes/common'], function(common) {
  //load libs and widgets for this page
  'use strict';

  require([
    'jquery'
    ],
    function($) {
      $('.hacker-nav__item').on('click', function() {
        console.log($('a' , this).text());
        //getNavLink($('a' , this).text());
      });

      /**
      * Retrieves the items for the clicked item
      * @param string $linkName the nav link name
      *
      */
      function getNavLink(linkName) {
        var navLinks = $.getJSON('/dist/js/links.json', function(json) {
          $.each(json[linkName], function(index, value) {
            console.log(value);
            if(value instanceof Array) {
              searchArray(value, linkName);
            }
          });
        });
      }

      /**
      * Searches for the appropriate nav item in the array
      * @param array $array
      * @param string $keyword
      * @return string
      *
      */
      function searchNavArray(array, keyword) {
        //searches for the keyword
      }
    });
});
